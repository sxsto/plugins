/**
* @author 哈尼
* @create_at 2022-09-10 12:08:30
* @title 百科
* @rule 百科 ?
* @rule ^-baike$
* @public false
* @description 🔍百科查询
* @version v1.0.0
*/

//请在消息框输入并发送：你好 佩奇
//建议同时打开浏览器控制台

//sender
const s = sender

//sender param 获取第1个参数
const nickname = s.param(1)
var option = {
    "method": "get",
    "url": "https://baike.baidu.com/search/word?word=" + encodeURI(nickname),
    "headers": {
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36 Edg/97.0.1072.55",

    }
}
request(option, (error, response) => {
    if (error) {
        s.reply(error)
    }
    var body = response.body.replace(/\r\n/g, "");
    body = body.replace(/\n/g, "");

    // 判断是否多个义项
    var subLemmaListTitle = body.match(/<div class="lemmaWgt-subLemmaListTitle">(.*?)<\/div>/)    
    
    if (subLemmaListTitle) {
        var items = body.match(/<li class="list-dot list-dot-paddingleft">(.*?)<\/li>/g)
        s.reply(`查找到${items.length}个结果，请自行前往查看：\n`)
        for (i = 0; i < items.length; i++){
            var title = items[i].match(/data-lemmaid="(\d+)">(.*?)<\/a>/)[2];
            
            var link = items[i].match(/href="(.*?)"/)[1];
            link = "https://baike.baidu.com" + link;

            s.reply(`${i+1}. ${title}\n${link}`)
        }
    } else {
        var item = body.match(/taskShare.init[(](.*?)[)];/)[1]
        var title = item.match(/title: '(.*?)'/)[1]
        var pic = item.match(/pic: '(.*?)'/)[1]
        pic = pic.replace(/\\/g, "")
        var fulldesc = body.match(/<div class="lemma-summary" label-module="lemmaSummary">(.*?)<div class="lemmaWgt-promotion-leadPVBtn">/)[1]
        fulldesc = fulldesc.replace(/<(.*?)>/g, "")
        fulldesc = fulldesc.replace(/&nbsp;/g, "")
        fulldesc = fulldesc.replace(/\[(\d+)\]/g, "")
        s.reply(title+"\n"+fulldesc+"\n"+image(pic))
    }
})
