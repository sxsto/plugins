/**
* @author sxsto
* @create_at 2022-09-10 12:08:30
* @description 🐒这个人很懒什么都没有留下。
* @title 随机图片
* @platform qq wx tg pgm web cron
* @rule ins
* @priority 100
* @admin true
* @public false
*/

//请在消息框输入并发送：你好 佩奇
//建议同时打开浏览器控制台


const s = sender

function main() {
    var { headers, status } = request ({
        //内置http请求函数
        url: "http://3650000.xyz/api/",
        method: "get",
        allowredirects: false,
        headers: {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36"
        }
    });
    if ([302, 304].indexOf(status) != -1) {
        s.reply(image(headers.Location))
    }
}

main()