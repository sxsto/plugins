/**
* @author YuanKK
* @create_at 2022-09-07 23:05:06
* @platform pgm web
* @title 买家秀3
* @rule ^买家秀3$
* @rule ^-?mjx$
* @public false
* @description 🔍买家秀，18🈲️
* @version v1.0.0
*/

const s = sender

function main() {
    var { headers, status } = request({
        url: "https://api.uomg.com/api/rand.img3?format=images",
        method: "get",
        allowredirects: false,
        headers: {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
        }
    });
    if ([302, 304].indexOf(status) != -1) {
        var id = s.reply(image(headers.Location))
        sleep(10000)
        s.recallMessage(s.getMessageId(), id)
    }
}

main()