/**
* @author cdle
* @create_at 2022-09-10 12:08:30
* @title 成语接龙
* @rule 成语接龙
* @rule ^-?cyjl$
* @public false
* @description 🐒经典成语接龙游戏，可以召唤小爱咬他。
* @version v1.0.0
*/

var begin = ""
var history = ""
var s = sender

function fword(cy) {
    history = begin
    var res = /([一-龥])】/.exec(cy)
    begin = res ? res[1] : ""
    begin = begin.replace(/】/g, "")
    return begin
}

function main() {
    var id = "" + new Date().getTime()
    var { body } = request("http://hm.suol.cc/API/cyjl.php?id=" + id + "&msg=开始成语接龙")
    var data = body
    fword(data)
    if (!begin) {
        return "游戏故障。"
    }
    s.reply(data)
    var stop = false
    var win = false
    var uid = s.getUserID()
    if (data.indexOf("你赢") != -1) {
        stop = true
        win = true
    }
    if (data.indexOf("我赢") != -1) {
        stop = true
    }
    s.listen(function (s) {
        if(stop) {
            return ""
        }
        var ct = s.getContent()
        var me = s.getUserId() == uid
        if (ct.indexOf("咬他") != -1) {
            var { message } = (new SillyGirl).session(`小爱${begin}字开头的成语有哪些？`)()
            res = []
            for (var cy of message.split("，")) {
                if (RegExp("^" + begin).exec(cy)) {
                    res.push(cy)
                    s.createSender({ content: cy, delay: 1000 })
                }
            }
            if (res.lenth == 0) {
                s.reply("小爱也不会呢，要不认输吧？")
            }else{
                s.reply(`接招吧，渣渣：\n${res.join("\n")}`)
            }
            return s.holdOn()
        }
        if (ct.indexOf("认输") != -1) {
            if (me || s.isAdmin()) {
                stop = true
                return
            } else {
                return s.holdOn("你不可以认输哦～")
            }
        }
        if (!RegExp("^" + begin).exec(ct) || ct.indexOf("接龙") != -1) {
            if (me && s.getImType() != "wxsv") {
                history
                if (RegExp("^" + history).exec(ct)) {
                    return s.holdOn()
                }
                return s.holdOn(`现在是接【${begin}】开头的成语哦，退出请对我说“认输”。`)
            } else {
                if (ct == "成语接龙") {
                    return s.holdOn(`现在是接【${begin}】开头的成语哦。`)
                }
                s.continue()
                return s.holdOn()
            }
        }
        data = request("http://hm.suol.cc/API/cyjl.php?id=" + id + "&msg=我接" + ct)
        data = data.body
        if (data.indexOf("file_get_contents") != -1) {
            return s.holdOn(data.split("\n").pop())
        }
        if (data.indexOf("你赢") != -1) {
            stop = true
            win = true
        } else if (data.indexOf("我赢") != -1) {
            stop = true
            win = false
        } else if (data.indexOf("恭喜") != -1) {
            fword(data)
        }
        uid = s.getUserID()
        if (!stop) {
            return s.holdOn(data)
        }
        return data
    }, "group")
    sleep(1000)
    if (!win) {
        return ("别灰心，下次再来比试一番！")
    }
    return ("下次我一定可以打败你～")
}

s.reply(main())