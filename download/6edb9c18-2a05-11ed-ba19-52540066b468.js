/**
* @author cdle
* @create_at 2022-09-10 12:08:30
* @title 猜成语
* @rule 猜成语
* @priority 1
* @public false
* @description 🐛看图猜成语
* @version v1.0.0
*/

var times = 0;
var s = sender;
var tip = 3;
function main() {
     times++;
     var data = request({ url: "https://api.iyk0.com/ktc/", "json": true });
     data = data.body
     var uid = s.getUserId();
     var uname = s.getUsername();
     if (!data || data.code != 200) {
          return "暂无成语可猜。"
     }
     var key = data.key
     if (times != 1) {
          s.reply(`${image(data.img)}\n回答正确，继续第${times}轮答题。`)
     } else {
          s.reply(`${image(data.img)}\n每次回答有60秒时间，超时退出游戏。`)
     }
     var tries = 0;
     var max = 5;
     while (tries < max) {
          s = s.await(60000, true);
          if (!s) {
               return `${uname} 答题超时，游戏结束。`
          }
          ct = s.getContent();
          if (ct == "猜成语") {
               s.reply(`${image(data.img)}\n答题进行中。`)
          } else if (ct == key) {
               return main();
          } else {
               if (s.getUserID() == uid) {
                    if (ct == "提示") {
                         if (tip > 0) {
                              tip--;
                              s.reply(`答案是“${key}”，剩余${tip}次提示机会。`);
                         } else {
                              s.reply(`提示机会已用完。`);
                         }
                    } else {
                         tries++;
                         if (max != tries)
                              s.reply(`回答错误，本轮剩余${max - tries}次答题机会。`);
                    }
               } else {
                    if (ct == "提示") {
                         s.reply(`上轮获胜者可以使用“提示”功能，剩余${tip}次提示机会。`);
                    } else {
                         s.continue();
                    }
               }
          }
     }
     s.reply(`正确答案是是${key}，${uname} ${max}次答题失败。`);
     return
}
s.reply(main())