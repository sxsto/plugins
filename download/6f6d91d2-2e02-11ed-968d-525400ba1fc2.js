/**
* @author sxsto
* @create_at 2022-09-07 23:52:26
* @title 油价查询
* @rule ?油价
* @priority 100
* @public false
* @description 🌲查询地区油价，支持省份查询
* @version v1.0.0
*/

const s = sender
const city = s.param(1)

var {body}= request({
    url: "https://api.xiao-xin.top/API/qiyou.php" + "?city=" + city, //请求链接
    method: "post",
    json: true,
});

if (body.code == 200) {
    data =
        "查询地区：" +
        city +
        "\n零号柴油：" +
        body.data["0号"] +
        "\n89号汽油：" +
        body.data["89号"] +
        "\n92号汽油：" +
        body.data["92号"] +
        "\n95号汽油：" +
        body.data["95号"] +
        "\n98号汽油：" +
        body.data["98号"] +
        "\n更新时间：" +
        body.data["updatetime"];
} else {
    data = "错误!请输入你要查询的省份!";
}

s.reply(data)